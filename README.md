# Description 描述
This project is for Chinese players to receive the update in game.
该储存库是为了给国人玩家提供的更新，我会时不时手动把最新版本搬运过来的。

# Why I create this project 为什么我要创造这个项目
Due to Chinese players can't connect to dropbox to detect and download update, I decide to create such a project to let Chinese players can get update from here.
因为dom维护的mod多使用dropbox为更新服务器，而我们中国国内的网络通常无法连接dropbox检测更新，因此我希望在此为各位提供一个可以自动检测更新的方式。
