{
    "name": "Extra Heist Info",
    "description": "",
    "author": "Dom",
    "contact": "http://steamcommunity.com/profiles/76561198078556212/",
    "version": "97",
    "blt_version" : 2,
    "priority" : -25,
    "supermod_definition" : "ehi.xml",
    "image" : "ehi.png",
    "color" : "0 0.58 1",
    "updates" :
    [
        {
            "identifier" : "pd2_extra_heist_info",
            "host":
            {
                "meta": "https://gitlab.com/LeftOwlRight/extra-heist-info/-/raw/main/meta.json"
            }
        }
    ]
}