local EHI = EHI
local Icon = EHI.Icons
local SF = EHI.SpecialFunctions
local TT = EHI.Trackers
local trigger_icon_all = { Icon.Defend }
local triggers = {
    [101034] = { id = "MikeDefendTruck", class = TT.Pausable, special_function = SF.UnpauseTrackerIfExistsAccurate, element = 101033 },
    [101038] = { id = "MikeDefendTruck", special_function = SF.PauseTracker },
    [101070] = { id = "MikeDefendTruck", special_function = SF.UnpauseTracker },

    [101535] = { id = "MikeDefendGarage", class = TT.Pausable, special_function = SF.UnpauseTrackerIfExistsAccurate, element = 101532 },
    [101534] = { id = "MikeDefendGarage", special_function = SF.UnpauseTracker },
    [101533] = { id = "MikeDefendGarage", special_function = SF.PauseTracker },

    [101048] = { time = 12, id = "ObjectiveDelay", icons = { Icon.Wait } }
}
if EHI:IsClient() then
    triggers[101034].time = 80
    triggers[101034].random_time = 10
    triggers[101034].special_function = SF.UnpauseTrackerIfExists
    triggers[101034].icons = trigger_icon_all
    triggers[101034].delay_only = true
    triggers[101034].class = TT.InaccuratePausable
    triggers[101034].synced = { class = TT.Pausable }
    EHI:AddSyncTrigger(101034, triggers[101034])
    triggers[101535].time = 90
    triggers[101535].random_time = 30
    triggers[101535].special_function = SF.UnpauseTrackerIfExists
    triggers[101535].icons = trigger_icon_all
    triggers[101535].delay_only = true
    triggers[101535].class = TT.InaccuratePausable
    triggers[101535].synced = { class = TT.Pausable }
    EHI:AddSyncTrigger(101535, triggers[101535])
end

local achievements =
{
    [101048] = { status = "defend", id = "born_3", class = TT.AchievementStatus, difficulty_pass = EHI:IsDifficultyOrAbove(EHI.Difficulties.OVERKILL) },
    [101001] = { status = "finish", id = "born_3", special_function = SF.SetAchievementStatus },
    [102777] = { id = "born_3", special_function = SF.SetAchievementComplete },
    [102779] = { id = "born_3", special_function = SF.SetAchievementFailed }
}

EHI:ParseTriggers({ mission = triggers, achievement = achievements }, nil, trigger_icon_all)
EHI:ShowLootCounter({ max = 9 })

local tbl =
{
    --units/payday2/equipment/gen_interactable_drill_small/gen_interactable_drill_small/001
    [101086] = { remove_vanilla_waypoint = true, waypoint_id = 101562 }
}
EHI:UpdateUnits(tbl)

local MissionDoorPositions =
{
    -- Workshop
    [1] = Vector3(-3798.92, -1094.9, -6.52779),

    -- Safe with bike mask
    [2] = Vector3(1570.02, -419.693, 185.724)
}
local MissionDoorIndex =
{
    [1] = { w_id = 101580 },
    [2] = { w_ids = { EHI:GetInstanceElementID(100007, 4850), EHI:GetInstanceElementID(100007, 5350) } }
}
EHI:SetMissionDoorPosAndIndex(MissionDoorPositions, MissionDoorIndex)